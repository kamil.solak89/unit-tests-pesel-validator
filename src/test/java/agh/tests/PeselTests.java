package agh.tests;

import agh.qa.Pesel;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PeselTests {

    private int expectedResult = -1;
    private Pesel peselToTestIndexLength;
    byte expectedValidResultUpperBVA = 5;
    byte expectedValidResultLowerBVA = 5;


    @BeforeMethod
    public void setUp() {
        peselToTestIndexLength = new Pesel(new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 5, 5});
    }

    @Test
    public void testGetDigitIndexTooBig() {
        Assert.assertEquals(peselToTestIndexLength.getDigit(12), expectedResult);
    }

    @Test
    public void testGetDigitNegativeIndexLength() {
        Assert.assertEquals(peselToTestIndexLength.getDigit(-1), expectedResult);
    }

    @Test
    public void testArrayIndexOutOfBoundsException() {
        Assert.assertEquals(peselToTestIndexLength.getDigit(11), expectedResult);
    }

    @Test
    public void testUpperBVA() {
        Assert.assertEquals(peselToTestIndexLength.getDigit(10), expectedValidResultUpperBVA);
    }

    @Test
    public void testLowerBVA() {
        Assert.assertEquals(peselToTestIndexLength.getDigit(0), expectedValidResultLowerBVA);
    }
}
