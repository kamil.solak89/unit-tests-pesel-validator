package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;

@Test
public class PeselExtractorTests {
    public void testGetBirthDate() {
        Pesel testBirthDate = new Pesel(new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 5, 5});
        LocalDate expectedLocalDate = LocalDate.of(1850, 06, 20);

        PeselExtractor peselExtractor = new PeselExtractor(testBirthDate);
        LocalDate extractedLocalDate = peselExtractor.getBirthDate();

        Assert.assertEquals(expectedLocalDate, extractedLocalDate);
    }

    @DataProvider
    public Object[][] getSexTestDataProvider() {
        return new Object[][]{
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 0, 5}, "Female"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 2, 5}, "Female"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 4, 5}, "Female"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 6, 5}, "Female"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 8, 5}, "Female"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 1, 5}, "Male"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 3, 5}, "Male"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 5, 5}, "Male"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 7, 5}, "Male"},
                {new byte[]{5, 0, 8, 6, 2, 0, 0, 2, 5, 9, 5}, "Male"},
        };
    }

    @Test(dataProvider = "getSexTestDataProvider")
    public void testGetSex(byte[] pesel, String expectedSex) {
        PeselExtractor peselExtractor = new PeselExtractor(new Pesel(pesel));
        String getActualSex = peselExtractor.getSex();

        Assert.assertEquals(getActualSex, expectedSex);
    }
}
